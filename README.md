Wom Demo App.

• Haciendo uso de la API pública https://dog.ceo/dog-api/ crear una app que muestre una lista
de las razas de perros disponibles con el siguiente api (https://dog.ceo/api/breeds/list/all)

• Al seleccionar una raza, se deben mostrar las imágenes asociadas a dicha raza, usando la
siguiente api (https://dog.ceo/api/breed/ {breed name}/images)


Para ejecutar el proyecto, solo es necesario hacer una sincronización de Gradle y ejecutar.
No hay mas configuraciones especiales.
