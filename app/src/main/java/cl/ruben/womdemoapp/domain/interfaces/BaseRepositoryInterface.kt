package cl.ruben.womdemoapp.domain.interfaces

import cl.ruben.womdemoapp.domain.entities.BreedEntity
import cl.ruben.womdemoapp.domain.entities.BreedImagesEntity
import cl.ruben.womdemoapp.infraestructure.Either
import cl.ruben.womdemoapp.infraestructure.WomException

interface BaseRepositoryInterface {
    suspend fun getBreedList() : Either<List<BreedEntity>, WomException>
    suspend fun getBreedImages(breedEntity: BreedEntity) : Either<BreedImagesEntity, WomException>
}