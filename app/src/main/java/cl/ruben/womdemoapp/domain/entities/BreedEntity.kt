package cl.ruben.womdemoapp.domain.entities

data class BreedEntity(
    val name: String,
    val variants : List<String>
)
data class BreedImagesEntity(
    val images : List<String>
)