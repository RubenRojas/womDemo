package cl.ruben.womdemoapp.domain.usecases

import cl.ruben.womdemoapp.domain.entities.BreedEntity
import cl.ruben.womdemoapp.domain.entities.BreedImagesEntity
import cl.ruben.womdemoapp.domain.interfaces.BaseRepositoryInterface
import cl.ruben.womdemoapp.infraestructure.Either
import cl.ruben.womdemoapp.infraestructure.WomException
import javax.inject.Inject

class BreedsUsecase @Inject constructor(
    private val repository: BaseRepositoryInterface
) {
    suspend fun getBreeds() : Either<List<BreedEntity>, WomException>{
        return repository.getBreedList()
    }

    suspend fun getBreedImages(breed: BreedEntity) : Either<BreedImagesEntity, WomException>{
        return repository.getBreedImages(breed)
    }
}