package cl.ruben.womdemoapp.infraestructure.di

import cl.ruben.womdemoapp.data.apiCall.ApiCaller
import cl.ruben.womdemoapp.data.mappers.ApiResponseModelMapper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent

@Module
@InstallIn(FragmentComponent::class)
class ApiCallerModule {
    @Provides
    fun provideApiCaller(): ApiCaller {
        return ApiCaller()
    }
}

@Module
@InstallIn(FragmentComponent::class)
class ApiResponseModelMapperModule {
    @Provides
    fun provideApiResponseMapper(): ApiResponseModelMapper {
        return ApiResponseModelMapper()
    }
}