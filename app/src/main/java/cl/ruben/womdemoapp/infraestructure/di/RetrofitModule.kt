package cl.ruben.womdemoapp.infraestructure.di

import cl.ruben.womdemoapp.data.repository.source.remote.api.BaseApi
import cl.ruben.womdemoapp.infraestructure.network.Endpoints
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(FragmentComponent::class)
class RetrofitModule {
    private val logging =
        HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC)
    private val client: OkHttpClient.Builder = OkHttpClient.Builder()


    @Provides
    fun baseApi(retrofit: Retrofit): BaseApi =
        retrofit.create(BaseApi::class.java)

    @Provides
    fun retrofit(): Retrofit {
        client.addInterceptor(logging)
        return Retrofit.Builder()
            .baseUrl(Endpoints.baseURL)
            .client(client.build())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

}