package cl.ruben.womdemoapp.infraestructure

sealed class Either<out OK, out ERROR> {
    data class Ok<out OK>(val data: OK) : Either<OK, Nothing>()
    data class Error<out ERROR>(val data: ERROR) : Either<Nothing, ERROR>()
}

