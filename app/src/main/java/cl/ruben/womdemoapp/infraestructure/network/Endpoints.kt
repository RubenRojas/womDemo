package cl.ruben.womdemoapp.infraestructure.network

class Endpoints {
    companion object{
        const val baseURL = "https://dog.ceo/api/"
        const val breedList = "breeds/list/all"
        fun imageList(breed : String) = "https://dog.ceo/api/breed/$breed/images"
    }
}