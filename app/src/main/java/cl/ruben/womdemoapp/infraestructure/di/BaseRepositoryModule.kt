package cl.ruben.womdemoapp.infraestructure.di

import cl.ruben.womdemoapp.data.repository.BaseRepository
import cl.ruben.womdemoapp.domain.interfaces.BaseRepositoryInterface
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent

@Module
@InstallIn(FragmentComponent::class)
abstract class BaseRepositoryModule {
    @Binds
    abstract fun bindBaseRepository(
        baseRepository: BaseRepository
    ): BaseRepositoryInterface
}