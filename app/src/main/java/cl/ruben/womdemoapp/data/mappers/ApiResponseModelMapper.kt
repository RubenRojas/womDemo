package cl.ruben.womdemoapp.data.mappers

import cl.ruben.womdemoapp.data.models.FetchApiResponseModel
import cl.ruben.womdemoapp.domain.entities.BreedEntity
import cl.ruben.womdemoapp.domain.entities.BreedImagesEntity

class ApiResponseModelMapper(){
    fun toListBreedEntity(input : FetchApiResponseModel): List<BreedEntity> {
        val list = mutableListOf<BreedEntity>()
        val breeds = input.message as Map<String, List<String>>
        for ((i, b) in breeds.keys.withIndex()) {
            val dog = BreedEntity(
                b,
                variants = breeds[b]!!
            )
            list.add(i, dog)
        }
        return list

    }

    fun toBreedImagesEntity(input : FetchApiResponseModel): BreedImagesEntity {
        return BreedImagesEntity(
            images = input.message as List<String>
        )
    }
}
