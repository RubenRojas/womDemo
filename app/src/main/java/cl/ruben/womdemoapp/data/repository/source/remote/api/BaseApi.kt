package cl.ruben.womdemoapp.data.repository.source.remote.api

import cl.ruben.womdemoapp.data.models.FetchApiResponseModel
import retrofit2.http.GET
import retrofit2.http.Url

interface BaseApi {
    @GET
    suspend fun getBreeds(
        @Url url: String,
    ): FetchApiResponseModel

    @GET
    suspend fun getImages(
        @Url url: String,
    ): FetchApiResponseModel


}