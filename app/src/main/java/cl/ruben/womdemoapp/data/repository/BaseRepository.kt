package cl.ruben.womdemoapp.data.repository

import cl.ruben.womdemoapp.data.apiCall.ApiCaller
import cl.ruben.womdemoapp.data.mappers.ApiResponseModelMapper
import cl.ruben.womdemoapp.data.repository.source.remote.RemoteSource
import cl.ruben.womdemoapp.domain.entities.BreedEntity
import cl.ruben.womdemoapp.domain.entities.BreedImagesEntity
import cl.ruben.womdemoapp.domain.interfaces.BaseRepositoryInterface
import cl.ruben.womdemoapp.infraestructure.Either
import cl.ruben.womdemoapp.infraestructure.WomException
import javax.inject.Inject

class BaseRepository @Inject constructor(
    private val source: RemoteSource,
    private val caller: ApiCaller,
    private val mapper: ApiResponseModelMapper
) : BaseRepositoryInterface {
    override suspend fun getBreedList(): Either<List<BreedEntity>, WomException> {
        return when (val data = caller.callNoParam(source::getBreedList)) {
            is Either.Error -> {
                // TODO : Specify the error message
                // i.e. : Network error, parse error, etc
                Either.Error(data = WomException("Error on getBreedList"))
            }

            is Either.Ok -> {
                Either.Ok(data = mapper.toListBreedEntity(data.data))
            }
        }
    }

    override suspend fun getBreedImages(breedEntity: BreedEntity): Either<BreedImagesEntity, WomException> {
        return when (val data =
            caller.call(breedEntity.name, source::getBreedImages)) {
            is Either.Error -> {
                // TODO : Specify the error message
                // i.e. : Network error, parse error, etc
                Either.Error(data = WomException("Error on getBreedList"))
            }

            is Either.Ok -> {
                Either.Ok(data = mapper.toBreedImagesEntity(data.data))
            }
        }
    }

}