package cl.ruben.womdemoapp.data.apiCall

import cl.ruben.womdemoapp.data.models.FetchApiResponseModel
import cl.ruben.womdemoapp.infraestructure.Either
import cl.ruben.womdemoapp.infraestructure.MAX_REINTENTOS_API
import cl.ruben.womdemoapp.infraestructure.WomException
import kotlin.reflect.KSuspendFunction0
import kotlin.reflect.KSuspendFunction1

/**
 * This class handle the retries when the network call fails.
 * Plus, return some information about the failure
 */
class ApiCaller {
    suspend fun <L> call(
        input: L,
        method: KSuspendFunction1<L, FetchApiResponseModel>
    ): Either<FetchApiResponseModel, WomException> {
        return try {
            Either.Ok(apiCall(input, 1, method))
        } catch (e: WomException) {
            Either.Error(e)
        }
    }

    suspend fun callNoParam(

        method: KSuspendFunction0<FetchApiResponseModel>
    ): Either<FetchApiResponseModel, WomException> {
        return try {
            Either.Ok(apiCallNoParam(1, method))
        } catch (e: WomException) {
            Either.Error(e)
        }
    }


    private suspend fun apiCallNoParam(
        tryIntent: Int,
        method: KSuspendFunction0<FetchApiResponseModel>
    ): FetchApiResponseModel {
        return try {
            val apiResult = method()
            if (apiResult.code == null) {
                return apiResult
            } else {
                throw WomException(apiResult.message as String)
            }

        } catch (e: Exception) {
            if (tryIntent < MAX_REINTENTOS_API) {
                apiCallNoParam(tryIntent + 1, method)
            } else {
                throw WomException("An error occur, try again later.\n[${e.message}]")
            }
        }
    }


    private suspend fun <L> apiCall(
        input: L,
        tryIntent: Int,
        method: KSuspendFunction1<L, FetchApiResponseModel>
    ): FetchApiResponseModel {
        return try {
            val apiResult = method(input)
            if (apiResult.code == null) {
                return apiResult
            } else {
                throw WomException(apiResult.message as String)
            }

        } catch (e: Exception) {
            if (tryIntent < MAX_REINTENTOS_API) {
                apiCall(input, tryIntent + 1, method)
            } else {
                throw WomException("An error occur, try again later.\n[${e.message}]")
            }
        }
    }

}