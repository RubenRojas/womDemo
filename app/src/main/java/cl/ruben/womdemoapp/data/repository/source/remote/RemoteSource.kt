package cl.ruben.womdemoapp.data.repository.source.remote

import cl.ruben.womdemoapp.data.models.FetchApiResponseModel
import cl.ruben.womdemoapp.data.repository.source.remote.api.BaseApi
import cl.ruben.womdemoapp.infraestructure.network.Endpoints
import javax.inject.Inject

class RemoteSource @Inject constructor(
    private val api: BaseApi,
) {

    suspend fun getBreedList(): FetchApiResponseModel {
        return api.getBreeds(Endpoints.breedList)
    }

    suspend fun getBreedImages(breed: String): FetchApiResponseModel {
        return api.getImages(Endpoints.imageList(breed))
    }
}