package cl.ruben.womdemoapp.data.models

data class FetchApiResponseModel(
    val status : String,
    val message : Any,
    val code : Int?
)