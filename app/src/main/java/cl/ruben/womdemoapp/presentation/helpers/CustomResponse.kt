package cl.ruben.womdemoapp.presentation.helpers

enum class Status {
    SUCCESS,
    ERROR,
    LOADING,
}
data class CustomResponse<out T>(
    val status: Status,
    val data: T? = null,
    val message: String = ""
) {
    companion object {
        fun <T> success(data: T): CustomResponse<T> =
            CustomResponse(status = Status.SUCCESS, data = data)

        fun <T> error(message: String): CustomResponse<T> =
            CustomResponse(status = Status.ERROR, message = message)

        fun <T> loading(): CustomResponse<T> = CustomResponse(status = Status.LOADING)
    }
}