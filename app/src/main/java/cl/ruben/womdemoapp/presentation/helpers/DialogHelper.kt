package cl.ruben.womdemoapp.presentation.helpers

import android.content.Context
import androidx.appcompat.app.AlertDialog
import cl.ruben.womdemoapp.R

class DialogHelper {
    companion object{
        private var loadingDialog : AlertDialog? = null
        private fun loadingDialog(context: Context?): AlertDialog {
            val builder = AlertDialog.Builder(
                context!!
            )
            builder.setCancelable(false)
            builder.setIcon(R.drawable.loading)
            builder.setView(R.layout.dialog_loading_dialog)
            return builder.create()
        }

        fun displayLoader(ctx : Context){
            loadingDialog = loadingDialog(ctx)
            loadingDialog!!.show()
        }
        fun hideLoader(){
            if(loadingDialog!= null &&  loadingDialog!!.isShowing){
                loadingDialog!!.hide()
            }
        }

        fun showErrorDialog(ctx : Context, content: String?) {
            val builder = AlertDialog.Builder(ctx)
            builder.setTitle("Error")
            builder.setCancelable(false)
            builder.setIcon(R.drawable.loading)
            builder.setMessage(content)
            builder.setPositiveButton("Ok", null)
            val dialog = builder.create()
            dialog.show()
        }
    }
}