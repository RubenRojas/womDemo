package cl.ruben.womdemoapp.presentation.mappers

import cl.ruben.womdemoapp.domain.entities.BreedEntity
import cl.ruben.womdemoapp.domain.entities.BreedImagesEntity
import cl.ruben.womdemoapp.presentation.presenters.BreedImagesPresenter
import cl.ruben.womdemoapp.presentation.presenters.BreedPresenter

fun BreedEntity.toBreedPresenter() : BreedPresenter{
    return BreedPresenter(
        name, variants
    )
}

fun List<BreedEntity>.toListBreedPresenter() : List<BreedPresenter>{
    return this.map { it.toBreedPresenter() }
}

fun BreedImagesEntity.toBreedImagesPresenter() : BreedImagesPresenter{
    return BreedImagesPresenter(
        images
    )
}

fun BreedPresenter.toBreedEntity() : BreedEntity{
    return BreedEntity( name, variants
    )
}