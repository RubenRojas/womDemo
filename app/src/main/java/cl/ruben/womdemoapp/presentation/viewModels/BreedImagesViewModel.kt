package cl.ruben.womdemoapp.presentation.viewModels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import cl.ruben.womdemoapp.domain.usecases.BreedsUsecase
import cl.ruben.womdemoapp.infraestructure.Either
import cl.ruben.womdemoapp.presentation.helpers.CustomResponse
import cl.ruben.womdemoapp.presentation.mappers.toBreedEntity
import cl.ruben.womdemoapp.presentation.mappers.toBreedImagesPresenter
import cl.ruben.womdemoapp.presentation.presenters.BreedPresenter
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class BreedImagesViewModel @Inject constructor(
    private val usecase: BreedsUsecase
) : ViewModel() {


    fun getBreedImages(breed: BreedPresenter) = liveData(Dispatchers.IO) {
        emit(CustomResponse.loading())
        when (val e = usecase.getBreedImages(breed.toBreedEntity())) {
            is Either.Error -> emit(CustomResponse.error(e.data.message))
            is Either.Ok -> emit(CustomResponse.success(e.data.toBreedImagesPresenter()))
        }
    }
}