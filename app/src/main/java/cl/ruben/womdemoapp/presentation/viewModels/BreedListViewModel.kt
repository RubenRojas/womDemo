package cl.ruben.womdemoapp.presentation.viewModels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import cl.ruben.womdemoapp.domain.usecases.BreedsUsecase
import cl.ruben.womdemoapp.infraestructure.Either
import cl.ruben.womdemoapp.presentation.helpers.CustomResponse
import cl.ruben.womdemoapp.presentation.mappers.toListBreedPresenter
import cl.ruben.womdemoapp.presentation.presenters.BreedPresenter
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class BreedListViewModel @Inject constructor(
    private val usecase: BreedsUsecase
) : ViewModel() {

    val breedList = MutableLiveData<List<BreedPresenter>>()
    fun init() = liveData(Dispatchers.IO) {
        if(breedList.isInitialized){
            breedList.postValue(breedList.value)
            emit(CustomResponse.success(true))
        }
        else{
            emit(CustomResponse.loading())
            getBreeds()
            emit(CustomResponse.success(true))
        }

    }

    private suspend fun getBreeds() {
        when (val e = usecase.getBreeds()) {
            is Either.Error -> breedList.postValue(listOf())
            is Either.Ok -> breedList.postValue(e.data.toListBreedPresenter())
        }
    }

}