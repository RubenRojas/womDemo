package cl.ruben.womdemoapp.presentation.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import cl.ruben.womdemoapp.databinding.FragmentBreedListBinding
import cl.ruben.womdemoapp.presentation.adapters.BreedItemAdapter
import cl.ruben.womdemoapp.presentation.helpers.DialogHelper
import cl.ruben.womdemoapp.presentation.helpers.Status
import cl.ruben.womdemoapp.presentation.helpers.hide
import cl.ruben.womdemoapp.presentation.helpers.observe
import cl.ruben.womdemoapp.presentation.helpers.show
import cl.ruben.womdemoapp.presentation.presenters.BreedPresenter
import cl.ruben.womdemoapp.presentation.viewModels.BreedListViewModel
import cl.ruben.womdemoapp.presentation.viewModels.BreedListViewModelFactory
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class BreedListFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: BreedListViewModelFactory
    lateinit var viewModel: BreedListViewModel
    lateinit var binding: FragmentBreedListBinding

    lateinit var breedItemAdapter: BreedItemAdapter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentBreedListBinding.inflate(
            LayoutInflater.from(
                requireContext()
            ), container, false
        )

        initViewModel()
        return binding.root
    }

    private fun initViewModel() {

        viewModel = ViewModelProvider(
            this,
            viewModelFactory
        )[BreedListViewModel::class.java]

        viewModel.run {
            observe(breedList, ::populateBreedList)
        }

        viewModel.init().observe(viewLifecycleOwner){
            when (it.status) {
                Status.SUCCESS -> {
                    DialogHelper.hideLoader()
                }
                Status.ERROR -> {
                    DialogHelper.hideLoader()
                    DialogHelper.showErrorDialog(requireContext(), it.message)
                }
                Status.LOADING -> DialogHelper.displayLoader(requireContext())
            }
        }
    }



    private fun populateBreedList(list: List<BreedPresenter>?) {
        if(!list.isNullOrEmpty()){
            binding.breedListNoData.hide()
            binding.breedList.show()

            breedItemAdapter = BreedItemAdapter(
                list,
                ::onBreedClickListener
            )
            binding.breedList.adapter = breedItemAdapter
        }
        else{
            binding.breedListNoData.show()
            binding.breedList.hide()
        }

    }

    private fun onBreedClickListener(breed: BreedPresenter) {
        val action = BreedListFragmentDirections.actionBreedListToBreedImages(breed)
        findNavController().navigate(action)
    }
}