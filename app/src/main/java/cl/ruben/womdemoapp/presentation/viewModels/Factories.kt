package cl.ruben.womdemoapp.presentation.viewModels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import cl.ruben.womdemoapp.domain.usecases.BreedsUsecase
import javax.inject.Inject

class BreedListViewModelFactory @Inject constructor(
    private val usecase: BreedsUsecase,
) : ViewModelProvider.Factory {
    override fun <T: ViewModel> create(modelClass: Class<T>): T {
        return BreedListViewModel(usecase) as T
    }
}

class BreedImagesViewModelFactory @Inject constructor(
    private val usecase: BreedsUsecase,
) : ViewModelProvider.Factory {
    override fun <T: ViewModel> create(modelClass: Class<T>): T {
        return BreedImagesViewModel(usecase) as T
    }
}