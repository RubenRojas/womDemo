package cl.ruben.womdemoapp.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cl.ruben.womdemoapp.databinding.BreedItemLayoutBinding
import cl.ruben.womdemoapp.presentation.presenters.BreedPresenter

class BreedItemAdapter(
    private val elements : List<BreedPresenter>,
    val listener : (BreedPresenter) -> Unit,
) : RecyclerView.Adapter<BreedItemAdapter.ViewHolder>()  {

    inner class ViewHolder(binding: BreedItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {

        val name = binding.breedItemLayoutName

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            BreedItemLayoutBinding.inflate(
                LayoutInflater.from(
                    parent.context,
                ), parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return elements.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = elements[position]
        holder.name.text = item.toDisplayText()
        holder.name.setOnClickListener {
            listener(item)
        }
    }

}