package cl.ruben.womdemoapp.presentation.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import cl.ruben.womdemoapp.databinding.FragmentBreedImagesBinding
import cl.ruben.womdemoapp.presentation.adapters.BreedImageItemAdapter
import cl.ruben.womdemoapp.presentation.helpers.DialogHelper
import cl.ruben.womdemoapp.presentation.helpers.Status
import cl.ruben.womdemoapp.presentation.presenters.BreedImagesPresenter
import cl.ruben.womdemoapp.presentation.presenters.BreedPresenter
import cl.ruben.womdemoapp.presentation.viewModels.BreedImagesViewModel
import cl.ruben.womdemoapp.presentation.viewModels.BreedImagesViewModelFactory
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class BreedImagesFragment : Fragment() {

    private val args: BreedImagesFragmentArgs by navArgs()

    @Inject
    lateinit var viewModelFactory: BreedImagesViewModelFactory
    lateinit var viewModel: BreedImagesViewModel
    lateinit var breed: BreedPresenter
    private lateinit var binding: FragmentBreedImagesBinding
    lateinit var adapter : BreedImageItemAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentBreedImagesBinding.inflate(
            LayoutInflater.from(
                requireContext()
            ), container, false
        )
        initView()
        initViewModel()
        getData()



        return binding.root
    }

    private fun initView(){
        binding.breedImagesBreedName.text = args.breed.toDisplayText()
    }
    private fun initViewModel() {

        viewModel = ViewModelProvider(
            this,
            viewModelFactory
        )[BreedImagesViewModel::class.java]
    }

    private fun getData() {
        breed = args.breed
        viewModel.getBreedImages(breed).observe(viewLifecycleOwner) {
            when (it.status) {
                Status.SUCCESS -> {
                    DialogHelper.hideLoader()
                    processData(it.data!!)
                }

                Status.ERROR -> {
                    DialogHelper.hideLoader()
                    DialogHelper.showErrorDialog(requireContext(), it.message)
                    closeThis()
                }

                Status.LOADING -> DialogHelper.displayLoader(requireContext())
            }
        }
    }

    private fun closeThis(){
        requireActivity().supportFragmentManager.beginTransaction().remove(this).commit()
    }


    private fun processData(images : BreedImagesPresenter){
        adapter = BreedImageItemAdapter(images.images)
        binding.breedImagesList.adapter = adapter
    }

}