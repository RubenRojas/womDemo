package cl.ruben.womdemoapp.presentation.presenters

import java.io.Serializable

data class BreedPresenter(
    val name: String,
    val variants : List<String>
) : Serializable{
    fun toDisplayText()  = name.uppercase()
}
data class BreedImagesPresenter(
    val images : List<String>
)