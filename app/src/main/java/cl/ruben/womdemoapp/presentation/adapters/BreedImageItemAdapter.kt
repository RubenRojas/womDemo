package cl.ruben.womdemoapp.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cl.ruben.womdemoapp.R
import cl.ruben.womdemoapp.databinding.BreedImageItemLayoutBinding
import com.squareup.picasso.Picasso

class BreedImageItemAdapter(
    private val elements: List<String>,
) : RecyclerView.Adapter<BreedImageItemAdapter.ViewHolder>() {

    inner class ViewHolder(binding: BreedImageItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {

        val image = binding.breedImage

    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        return ViewHolder(
            BreedImageItemLayoutBinding.inflate(
                LayoutInflater.from(
                    parent.context,
                ), parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return elements.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = elements[position]
        Picasso.get()
            .load(item)
            .placeholder(R.drawable.placeholder)
            .error(R.drawable.image_error)
            .into(holder.image)
    }

}