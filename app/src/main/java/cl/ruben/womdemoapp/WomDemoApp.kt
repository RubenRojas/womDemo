package cl.ruben.womdemoapp

import android.app.Application
import android.content.Context
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class WomDemoApp : Application() {

    init {
        instance = this
    }

    companion object {
        var instance: WomDemoApp? = null
        var currentId: Int = -1

        fun context(): Context {
            return instance!!.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()
        val context: Context = WomDemoApp.context()
    }
}